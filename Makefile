all: proposal.pdf

.PHONY: all

proposal.pdf: proposal.tex
	pdflatex proposal.tex

clean:
	git clean -fdx
